# Social Medias

I mostly use Discord, so if you're trying to contact me, please use Discord or Facebook Messenger.

**Discord**: Raymond#2829

**Twitter**: [@RaymondTheOof](https://twitter.com/RaymondTheOof)

**YouTube**: [RaymondHsu](https://youtube.com/RaymondHsu)

**Facebook**: [ush.dnomyar](https://facebook.com/ush.dnomyar)

**Instagram**: [ush.dnomyar](https://instagram.com/ush.dnomyar)

**Github**: [raymond-1227](https://github.com/raymond-1227)

**GitLab**: [raymond-1227](https://gitlab.com/raymond-1227)

**Hypixel**: [dnomyaR](https://hypixel.net/members/dnomyar.1811695)
