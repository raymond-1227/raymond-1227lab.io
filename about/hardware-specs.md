# Hardware Specs

Here are the computer and phone specs which I use every day.

## Desktop Specs

### Computer

**Model**: Apple iMac (Retina 5K, 27-inch, 2019) (A2115)

**Year of Manufacture**: Mid 2020

**CPU**: Intel Core i5-8500

**Storage**: Fusion Drive 2TB + Seagate HDD 6TB (For Time Machine)

**Optical Drive**: Not Included

**RAM**: 24GB 2667 MHz DDR4 SO-DIMM (Apple 4GB x2 + Crucial 16GB x1)

**GPU**: AMD Radeon Pro 570X with 4GB GDDR5 Graphics Memory

**Webcam**: FaceTime HD Camera (Built-in)

**Operating System**: macOS Monterey 12.2 | Windows 11 Pro (Boot Camp)

### Computer Accessories

**Keyboard**: Magic Keyboard (It's obvious that many Windows keyboards have different keymap compared to Mac ones)

**Mouse**: Magic Mouse 2 (Yes I know you hate this, but it has great scrolling features) / Logitech M-U0027-O (It's just a business mouse I use for gaming, and it doesn't give you decent CPS for sure)

**Headset**: AKG K702 Reference Studio Headset

**Microphone**: Zoom H4n Handy Recorder

## Mobile Specs

### Phone

**Model**: realme 7 5G (RMX2111)

**Year of Manufacture**: Late 2020

**CPU**: MediaTek MT6853 Dimensity 800U (2x2.4 GHz Cortex-A76 & 6x2.0 GHz Cortex-A55)

**RAM**: 8GB 2133 MHz LPDDR4X

**Storage**: ROM 128GB (UFS 2.1) + SD Card 64GB

**GPU**: Mali-G57 MC3

**Camera**: Rear (48 MP Main, 8 MP Ultra-Wide, 2MP Macro, 2 MP Depth) + Selfie (16 MP)

**Operating System**: realme UI 2.0 (Android 11, based on Color OS 11) (RMX2111_11_C.08)

### Tablet

**Model**: Apple iPad Pro (A1584)

**Year of Manufacture**: Late 2015

**Chipset**: Apple A9X

**Storage**: 128GB Flash Memory

**RAM**: 4GB 1600 MHz LPDDR4

**Camera**: Rear (8 MP) + Selfie (1.2 MP)

**Operating System**: iPadOS 15.3

## Camera Specs

*(Note: I don't own this camera, my dad owns it. Nice build quality BTW.)*

**Camera Model**: Canon EOS 5D Mark II

**Year of Manufacture**: Late 2008

**Image Sensor Size**: 36 x 24 mm (full-frame)

**Maximum Resolution**: 5,616 × 3,744 px (21.1 MP)

**Shutter Speed**: 30 to 1/8000 Seconds

**Storage**: CompactFlash

**Compatible Lens I Use**: EF 70-200mm f/2.8L IS II USM, EF 35mm f/1.4L USM
