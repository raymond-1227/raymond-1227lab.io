module.exports = {
    dest: "public",
    title: "Raymond's Site",
    description: "Basically some informations about me (or related stuffs)",
    head: [
        ['link', { rel: 'icon', href: '/.vuepress/public/images/favicon.png' }],
    ],
    themeConfig: {
        nav: [
            { text: "Home", link: "/" },
            { text: "About Me", link: "/about/" },
            { text: "My Projects", link: "/my-projects/" },
            { text: "New Website", link: "https://raymond.pages.dev" },
        ],
        sidebar: {
            "/about/": [
                "",
                "my-bio",
                "hardware-specs",
                "social-medias",
                "donate"
            ],
            "/my-projects/": [
                "",
            ]
        }
    }
}
