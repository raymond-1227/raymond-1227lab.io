---
home: true
heroImage: /images/portrait.jpg
heroText: Raymond Hsu
tagline: A kid who doesn't how to code properly.
actionText: About Me →
actionLink: /about/
features:
- title: Photography
  details: Unlike the beauty camers today, I rather upload pure unedited photos.
- title: Coding
  details: I need the advanced-coding-knowledge.js package added to my brain.
- title: Important Note
  details: This website is no longer being maintained, links may eventually break.
footer: Copyright © 2020-present Raymond Hsu
---
